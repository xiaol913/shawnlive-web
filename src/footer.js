import React, {Component} from 'react';
import {Button, Row, Col, Popover} from 'antd';

import footer from './footer.module.css';
import wechat_png from './wechat.png';
import qq_png from './qq.png';

class Footer extends Component {
    render() {

        const wechat = (
            <div className={footer.content}>
                <img src={wechat_png} alt={'my wechat'}/>
            </div>
        );

        const qq = (
            <div className={footer.content}>
                <img src={qq_png} alt={'my qq'}/>
            </div>
        );

        return (
            <footer>
                <Row gutter={16} type="flex" justify="space-around" align="middle" className={footer.row}>
                    <Col span={7}/>
                    <Col span={2}>
                        <Button ghost shape="circle" href={'https://github.com/xiaol913'} icon={'github'}
                                className={footer.button}/>
                    </Col>
                    <Col span={2}>
                        <Button ghost shape="circle" href={'https://gitlab.com/xiaol913'} icon={'gitlab'}
                                className={footer.button}/>
                    </Col>
                    <Col span={2}>
                        <Button ghost shape="circle" href={'http://shawnlive.com'} icon={'home'}
                                className={footer.button}/>
                    </Col>
                    <Col span={2}>
                        <Popover content={qq} title={'My QQ QR code'} placement="top" trigger={'hover'}>
                            <Button ghost shape="circle" icon={'qq'} className={footer.button}/>
                        </Popover>
                    </Col>
                    <Col span={2}>
                        <Popover content={wechat} title={'My Wechat QR code'} placement="top" trigger={'hover'}>
                            <Button ghost shape="circle" icon={'wechat'} className={footer.button}/>
                        </Popover>
                    </Col>
                    <Col span={7}/>
                </Row>
            </footer>
        );
    }
}

export default Footer;