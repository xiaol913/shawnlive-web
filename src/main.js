import React, {Component} from 'react';
import {Row, Col, Button, Drawer, Divider} from 'antd';

import main from './main.module.css';
import Header from './header';
import Footer from './footer';
import {getAbout} from './api';

const DescriptionItem = ({title, content}) => (
    <div className={main.desc}>
        <p>
            {title}:
        </p>
        {content}
    </div>
);


const AboutMe = ({about,}) => (
    <div>
        <p className={main.about} style={{marginBottom: 24}}>About Me</p>
        <Row>
            <Col span={12}>
                <DescriptionItem title="Full Name" content="Xiang Gao"/>{' '}
            </Col>
        </Row>
        <Row>
            <Col span={12}>
                <DescriptionItem title="City" content={about['city']}/>
            </Col>
            <Col span={12}>
                <DescriptionItem title="Country" content="China🇨🇳"/>
            </Col>
        </Row>
        <Row>
            <Col span={12}>
                <DescriptionItem title="Birthday" content="** **, 1987"/>
            </Col>
            <Col span={12}>
                <DescriptionItem title="Website" content="Shawnlive.com"/>
            </Col>
        </Row>
        <Row>
            <Col span={24}>
                <DescriptionItem
                    title="Hobby"
                    content={about['hobby']}
                />
            </Col>
        </Row>
        <Divider/>
        <p className={main.about}>Company</p>
        <Row>
            <Col span={12}>
                <DescriptionItem title="Position" content={about['position']}/>
            </Col>
            <Col span={12}>
                <DescriptionItem title="Responsibilities" content={about['responsibilities']}/>
            </Col>
        </Row>
        <Row>
            <Col span={12}>
                <DescriptionItem title="Department" content={about['department']}/>
            </Col>
        </Row>
        <Row>
            <Col span={24}>
                <DescriptionItem
                    title="Python"
                    content={about['python']}
                />
            </Col>
        </Row>
        <Row>
            <Col span={24}>
                <DescriptionItem
                    title="Web"
                    content={about['web']}
                />
            </Col>
        </Row>
        <Row>
            <Col span={24}>
                <DescriptionItem
                    title="Server"
                    content={about['server']}
                />
            </Col>
        </Row>
        <Row>
            <Col span={24}>
                <DescriptionItem
                    title="Others"
                    content={about['others']}
                />
            </Col>
        </Row>
        <Divider/>
        <p className={main.about}>Contacts</p>
        <Row>
            <Col span={12}>
                <DescriptionItem title="Email" content={about['email']}/>
            </Col>
        </Row>
        <Row>
            <Col span={12}>
                <DescriptionItem title="QQ" content={about['qq']}/>
            </Col>
        </Row>
        <Row>
            <Col span={12}>
                <DescriptionItem title="Wechat" content={about['wechat']}/>
            </Col>
        </Row>
    </div>
);

class Main extends Component {

    constructor() {
        super();
        this.state = {
            projectLoading: false,
            blogLoading: false,
            photoLoading: false,
            drawer: false,
            about: ''
        };
    };

    showDrawer = () => {
        this.setState({drawer: true});
    };

    onClose = () => {
        this.setState({drawer: false});
    };

    projectIconLoading = () => {
        this.setState({projectLoading: true});
    };

    blogIconLoading = () => {
        this.setState({blogLoading: true});
    };

    photoIconLoading = () => {
        this.setState({photoLoading: true});
    };

    componentDidMount() {
        getAbout()
            .then(response => {
                this.setState({
                    about: response.data
                });
            })
            .catch(error => {
                console.error('error', error);
            });
    };

    render() {
        return (
            <div className={main.container}>
                <Header/>
                <Row className={main.row} gutter={56} type="flex" justify="space-around" align="middle">
                    <Col span={6}/>
                    <Col span={3} className={main["col-item"]}>
                        <div>
                            <Button ghost icon={'code-o'} loading={this.state.projectLoading}
                                    onClick={this.projectIconLoading} className={main.button} href={'/projects'}/>
                            <p>Projects</p>
                        </div>
                    </Col>
                    <Col span={3} className={main["col-item"]}>
                        <div>
                            <Button ghost icon={'form'} loading={this.state.blogLoading}
                                    onClick={this.blogIconLoading} className={main.button} href={'/blog'}/>
                            <p>Blog</p>
                        </div>
                    </Col>
                    <Col span={3} className={main["col-item"]}>
                        <div>
                            <Button ghost icon={'picture'} loading={this.state.photoLoading}
                                    onClick={this.photoIconLoading} className={main.button} href={'/photos'}/>
                            <p>Photos</p>
                        </div>
                    </Col>
                    <Col span={3} className={main["col-item"]}>
                        <div>
                            <Button ghost className={main.button} onClick={this.showDrawer} icon={'smile-o'}/>
                            <p>About Me</p>
                            <Drawer
                                width={'40%'}
                                placement={'right'}
                                closable={true}
                                onClose={this.onClose}
                                visible={this.state.drawer}
                            >
                                <AboutMe about={this.state.about}/>
                            </Drawer>
                        </div>
                    </Col>
                    <Col span={6}/>
                </Row>
                <Footer/>
            </div>
        );
    }
}

export default Main;