import React, {Component} from 'react';

import './header.module.css';
import logo from './logo.png';

class Header extends Component{
    render(){
        return(
            <header>
                <img src={logo} alt={'ShawnLive'}/>
            </header>
        );
    }
}

export default Header;