import React, {Component} from 'react';
import {Drawer, Button, Badge, Affix} from 'antd';

import main from './info.module.css';
import Header from './header';
import Footer from './footer';
import Comment from './comment';
import {getBlogInfo} from './api';

class Info extends Component {

    constructor() {
        super();
        this.state = {
            blog: '',
            visible: false,
            comments: ''
        };
    };

    componentDidMount() {
        getBlogInfo(this.props.match.params.id)
            .then(response => {
                this.setState({
                    blog: response.data,
                    comments: response.data['comment'].length
                });
            })
            .catch(error => {
                console.error('error', error)
            });
    };

    static createMarkup(desc) {
        return {__html: desc}
    }

    showDrawer = () => {
        this.setState({
            visible: true,
        });
    };

    onClose = () => {
        this.setState({
            visible: false,
        });
    };

    render() {
        return (
            <div className={main.main}>
                <Header/>
                <section ref={(node) => { this.container = node; }}>
                    <Affix target={() => this.container} className={main.back}>
                        <Button icon={'left'} href={'/blog'}>
                            返回博客列表
                        </Button>
                    </Affix>
                    <title>{this.state.blog['title']}</title>
                    <article>
                        <div dangerouslySetInnerHTML={Info.createMarkup(this.state.blog['content'])}/>
                    </article>
                    <Button onClick={this.showDrawer}>
                        <Badge count={this.state.comments} showZero>查看评论</Badge>
                    </Button>
                    <Drawer
                        width={'60%'}
                        placement="right"
                        onClose={this.onClose}
                        maskClosable={false}
                        visible={this.state.visible}
                    >
                        <Comment id={this.props.match.params.id}/>
                    </Drawer>
                </section>
                <Footer/>
            </div>
        );
    }
}

export default Info;