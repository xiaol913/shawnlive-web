import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';

import index from './index.module.css';
import Main from './main';
import Projects from './projects';
import Photos from './photos';
import Blog from './blog';
import Info from './info';
import * as serviceWorker from './serviceWorker';

class Index extends Component {
    render() {
        return (
            <Router>
                <div className={index.main}>
                    <Switch>
                        <Route exact path={'/'} component={Main}/>
                        <Route path={'/projects'} component={Projects}/>
                        <Route path={'/photos'} component={Photos}/>
                        <Route exact path={'/blog'} component={Blog}/>
                        <Route path={'/blog/:id'} component={Info}/>
                    </Switch>
                </div>
            </Router>
        );
    }
}

ReactDOM.render(<Index/>, document.getElementById('index'));
serviceWorker.unregister();
