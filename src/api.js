import axios from 'axios';

let host = 'http://api.shawnlive.com/';
let instance = axios.create({
    baseURL: host,
    timeout: 1000,
    headers: {
        'Content-Type': 'application/json',
    },
});

export const getBlog = () => {
    return instance.get(`blog/`)
};

export const getBlogInfo = (blog_id) => {
    return instance.get(`blog/${blog_id}`)
};

export const getComment = (blog_id) => {
    return instance.get(`comment/?blog=${blog_id}`)
};

export const postComment = (data) => {
    return instance.post(`comment/`, data, {
        headers: {
            'Content-Type': 'application/json'
        },
    })
};

export const getPhoto = () => {
    return instance.get(`photos/`)
};

export const getProjects = () => {
    return instance.get(`projects/`)
};

export const getAbout = () => {
    return instance.get(`about/1`)
};