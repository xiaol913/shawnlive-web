import React, {Component} from 'react';
import {Spin} from 'antd'

import photos from './photos.module.css';
import Footer from './footer';
import {getPhoto} from './api';

class Photos extends Component {

    constructor() {
        super();
        this.state = {
            pics: '',
            prev: 0,
            cur: 0,
            next: 0,
        };
    };

    componentDidMount() {
        getPhoto()
            .then(response => {
                this.setState({
                    pics: response.data
                });
                if (response.data.length >= 2) {
                    this.setState({
                        prev: response.data.length - 1,
                        next: 1,
                    })
                }
            })
            .catch(error => {
                console.error('error', error);
            });
    };

    prevImg() {
        let img = document.getElementsByClassName(`${photos.child}`);
        for (let i = 0; i < img.length; i++) {
            // console.log(img[i]);
            img[i].style.opacity = '0';
        }
        setTimeout(
            () => {
                if (this.state.prev === 0) {
                    this.setState({
                        next: this.state.cur,
                        cur: this.state.prev,
                        prev: this.state.pics.length - 1,
                    })
                } else {
                    this.setState({
                        next: this.state.cur,
                        cur: this.state.prev,
                        prev: this.state.prev - 1,
                    })
                }
            },
            1000
        );
    };

    nextImg() {
        let img = document.getElementsByClassName(`${photos.child}`);
        for (let i = 0; i < img.length; i++) {
            // console.log(img[i]);
            img[i].style.opacity = '0';
        }
        setTimeout(
            () => {
                if (this.state.next === this.state.pics.length - 1) {
                    this.setState({
                        prev: this.state.cur,
                        cur: this.state.next,
                        next: 0,
                    })
                } else {
                    this.setState({
                        prev: this.state.cur,
                        cur: this.state.next,
                        next: this.state.next + 1,
                    })
                }
            },
            1000
        );
    };

    render() {
        const {pics} = this.state;
        const picList = pics.length
            ? pics.map((pic, index) => (
                <div key={index} className={photos.child}>
                    <div className={photos.article}>
                        <div>
                            <h3>{pic['title']}</h3>
                        </div>
                        <div>
                            <h4>{pic['dec']}</h4>
                        </div>
                    </div>
                    <div className={photos["mid-pic"]}>
                        <img src={pic['img']} alt={'标题'}/>
                    </div>
                </div>
            ))
            : <div><Spin size={"large"}/></div>;

        return (
            <div style={{height: '100%'}}>
                <div className={photos.photos}>
                    <div onClick={this.prevImg.bind(this)}
                         className={photos["photo-left"]}>{picList[this.state.prev]}</div>
                    <div className={photos["photo-mid"]}>{picList[this.state.cur]}</div>
                    <div onClick={this.nextImg.bind(this)}
                         className={photos["photo-right"]}>{picList[this.state.next]}</div>
                </div>
                <Footer/>
            </div>
        );
    }
}

export default Photos;