import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import {List, Tag, Badge, Button} from 'antd';

import main from './blog.module.css';
import Header from './header';
import Footer from './footer';
import {getBlog} from './api';

class Blog extends Component {

    constructor() {
        super();
        this.state = {
            blog: ''
        };
    };

    componentDidMount() {
        getBlog()
            .then(response => {
                this.setState({
                    blog: response.data
                });
            }).catch(error => {
                console.error('error', error);
        });
    };

    static labelTag(label, count, time, view) {
        let color = "green";
        let content = "others";
        if (label === 1) {
            color = "gold";
            content = 'python'
        } else if (label === 2) {
            color = "magenta";
            content = 'web'
        } else if (label === 3) {
            color = "geekblue";
            content = 'server'
        }
        return (
            <div>
                <Tag color={color}>{content}</Tag>
                <Tag color={'#87d068'}>阅读数：{view}</Tag>
                <Badge count={count} showZero>
                    <Tag color={'lime'}>评论</Tag>
                </Badge>
                &nbsp;&nbsp;&nbsp;&nbsp;
                <Tag color={'volcano'}>发表于：{time}</Tag>
            </div>
        )
    };

    render() {

        return (
            <div className={main.main}>
                <Header/>
                <section>
                    <List
                        dataSource={this.state.blog}
                        renderItem={
                            item => (
                                <List.Item
                                    key={item['title']}
                                    actions={[<Link to={`/blog/${item['id']}`} replace><Button ghost
                                                                                               icon={'menu-unfold'}>View</Button></Link>]}
                                >
                                    <List.Item.Meta
                                        title={item['title']}
                                        description={Blog.labelTag(item['label'], item['comment'].length, item['create_date'], item['view'])}
                                    />
                                </List.Item>
                            )
                        }/>
                </section>
                <Footer/>
            </div>
        );
    }
}

export default Blog;