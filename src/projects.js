import React, {Component} from 'react';
import {Card, Row, Col, Button, Popover, Spin} from 'antd';

import main from './projects.module.css';
import Header from './header';
import Footer from './footer';
import {getProjects} from './api';

const {Meta} = Card;

class Projects extends Component {

    constructor() {
        super();
        this.state = {
            projects: ''
        };
    };

    static createMarkup(desc) {
        return {__html: desc}
    }

    componentDidMount() {
        getProjects()
            .then(response => {
                this.setState({
                    projects: response.data
                })
            })
            .catch(error => {
                console.error('error', error);
            });
    };

    render() {
        const {projects} = this.state;
        const proList = projects.length
            ? projects.map((proItem, index) => (
                <Col key={index} span={5}>
                    <Card
                        hoverable
                        actions={[
                            <Popover content={
                                <div>
                                    <div dangerouslySetInnerHTML={Projects.createMarkup(proItem['desc'])}/>
                                    Click <a href={proItem['url']}>here</a>!
                                </div>
                            }>
                                <Button className={main.button} icon={'info-circle-o'}
                                        href={proItem['url']}/>
                            </Popover>
                        ]}
                    >
                        <Meta
                            title={proItem['name']}
                            description={proItem['intro']}
                        />
                    </Card>
                </Col>
            ))
            : <div><Spin size={"large"}/></div>;
        return (
            <div className={main.container}>
                <Header/>
                <Row className={main.row} gutter={56} type="flex" justify="space-around" align="middle">
                    {proList}
                </Row>
                <Footer/>
            </div>
        );
    }
}

export default Projects;