import React, {Component} from 'react';
import {Form, Input, Card, Row, Col, Button, message} from 'antd';

import {getComment, postComment} from './api';

const FormItem = Form.Item;
const {TextArea} = Input;

class Comment extends Component {

    constructor() {
        super();
        this.state = {
            comments: '',
            visible: ''
        };
    };

    componentDidMount() {
        getComment(this.props.id)
            .then(response => {
                this.setState({
                    comments: response.data
                });
            })
            .catch(error => {
                console.error('error', error);
            });
    };

    static handleSubmit(e) {
        e.preventDefault();
        let data = this.props.form.getFieldsValue();
        data['blog'] = this.props.id;
        postComment(data)
            .then(() => {
                message.success('评论发表成功');
                this.setState({visible: 'none'});
                this.componentDidMount();
            })
            .catch(error => {
                console.error('error', error);
            });
    };

    render() {
        const {getFieldDecorator} = this.props.form;
        const {comments} = this.state;
        const commentList = comments.length
            ? comments.map((comment, index) => (
                <Card key={index} title={comment['name']} extra={<p>发布于：{comment['create_date']}</p>}>
                    <p>{comment['content']}</p>
                </Card>))
            : '没有评论';

        return (
            <div>
                <Row style={{marginBottom: 20}}>
                    <Col span={24} style={{display: this.state.visible}}>
                        <Form onSubmit={Comment.handleSubmit.bind(this)}>
                            <FormItem label={'您的姓名'}>
                                {getFieldDecorator('name', {})(
                                    <Input placeholder="随便写"/>
                                )}
                            </FormItem>
                            <FormItem label={'您的评论'}>
                                {getFieldDecorator('content', {})(
                                    <TextArea autosize placeholder="随便写"/>
                                )}
                            </FormItem>
                            <Button type="primary" htmlType="submit">
                                提交评论
                            </Button>
                        </Form>
                    </Col>
                </Row>
                {commentList}
            </div>
        );
    }
}

export default Comment = Form.create({})(Comment);